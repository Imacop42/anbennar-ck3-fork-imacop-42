﻿elector_voting_pattern_circumstances_elven_elective_modifier = {
	##########################	Elector voting patterns (circumstances)	##########################
	#Stats
	#Age
	
	#Stats
	modifier = {	# Candidate's Diplomacy
		desc = tooltip_elven_elector_vote_diplomacy
		scope:candidate = { diplomacy >= 10 }
		add = {
			value = 5
			if = {
				limit = {
					scope:candidate = { diplomacy > 12 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { diplomacy > 16 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { diplomacy > 18 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { diplomacy > 20 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { diplomacy > 25 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { diplomacy > 30 }
				}
				add = 5
			}
		}
	}
	modifier = {	# Candidate's Martial
		desc = tooltip_elven_elector_vote_martial
		scope:candidate = { martial >= 10 }
		add = {
			value = 5
			if = {
				limit = {
					scope:candidate = { martial > 12 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { martial > 16 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { martial > 18 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { martial > 20 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { martial > 25 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { martial > 30 }
				}
				add = 5
			}
		}
	}
	modifier = {	# Candidate's Stewardship
		desc = tooltip_elven_elector_vote_stewardship
		scope:candidate = { stewardship >= 10 }
		add = {
			value = 5
			if = {
				limit = {
					scope:candidate = { stewardship > 12 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { stewardship > 16 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { stewardship > 18 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { stewardship > 20 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { stewardship > 25 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { stewardship > 30 }
				}
				add = 5
			}
		}
	}
	modifier = {	# Candidate's Intrigue
		desc = tooltip_elven_elector_vote_intrigue
		scope:candidate = { intrigue >= 10 }
		add = {
			value = 5
			if = {
				limit = {
					scope:candidate = { intrigue > 12 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { intrigue > 16 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { intrigue > 18 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { intrigue > 20 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { intrigue > 25 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { intrigue > 30 }
				}
				add = 5
			}
		}
	}
	modifier = {	# Candidate's Learning
		desc = tooltip_elven_elector_vote_learning
		scope:candidate = { learning >= 10 }
		add = {
			value = 5
			if = {
				limit = {
					scope:candidate = { learning > 12 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { learning > 16 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { learning > 18 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { learning > 20 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { learning > 25 }
				}
				add = 5
			}
			if = {
				limit = {
					scope:candidate = { learning > 30 }
				}
				add = 5
			}
		}
	}
	
	#Age
	modifier = {
		desc = tooltip_feudal_elector_vote_baby
		NOT = { this = scope:candidate } #Not on yourself.
		scope:candidate = {
			age < 5
		}
		add = -50
	}
	modifier = {
		desc = tooltip_feudal_elector_vote_child
		NOT = { this = scope:candidate } #Not on yourself.
		scope:candidate = {
			age >= 5
			age < 16
		}
		add = -40
	}
	modifier = {
		desc = tooltip_feudal_elector_vote_youngster
		NOT = { this = scope:candidate } #Not on yourself.
		scope:candidate = {
			age >= 16
			age < 150
		}
		add = {
			subtract = 10
			if = {
				limit = {
					age < 100
				}
				subtract = 10
			}
			if = {
				limit = {
					age < 50
				}
				subtract = 10
			}
		}
	}
	modifier = {
		desc = tooltip_elven_elector_vote_old
		scope:candidate = {
			age >= 250
		}
		add = {
			value = 10
			if = {
				limit = {
					age >= 300
				}
				add = 10
			}
			if = {
				limit = {
					age >= 350
				}
				add = 10
			}
		}
	}
}