﻿anb_dominate_spell = {
	skill = learning
	hostile = yes
	uses_agents = no
	cooldown = {
		years = 10
	}
	is_secret = yes
	base_secrecy = -10
	power_per_skill_point = 5
	resistance_per_skill_point = 0
	spymaster_power_per_skill_point = 0
	spymaster_resistance_per_skill_point = 0
	minimum_progress_chance = 10
	maximum_success = 60
	maximum_secrecy = 85
	allow = {
		is_adult = yes
		is_landed = yes
		is_imprisoned = no
	}
	valid = {
		scope:target = {
			is_adult = yes
			OR = {
				is_imprisoned = no
				is_imprisoned_by = scope:owner
			}
			NOT = {
				has_trait = incapable
			}
			exists = location
		}
		custom_description = {
			text = already_has_strong_hook_on_target
			NOT = {
				scope:owner = {
					has_strong_hook = scope:target
				}
			}
		}
		custom_description = {
			text = anb_in_spell_range
			scope:target = {
				liege_or_court_owner = scope:owner.liege_or_court_owner
			}
		}
		# Special AI exceptions
		NOT = {
			scope:owner = {
				is_ai = yes
				has_opinion_modifier = {
					target = scope:target
					modifier = repentant_opinion
				}
			}
		}
	}
	base_success_chance = {
		base = 5
		# CASTER #
		#Magical affinity level
		modifier = {
			add = 20
			scope:owner = {
				has_trait = magical_affinity_2
			}
		}
		modifier = {
			add = 40
			scope:owner = {
				has_trait = magical_affinity_3
			}
		}
		#How well do they know the target
		modifier = {
			add = 10
			scope:owner = {
				has_relation_friend = scope:target
			}
		}
		modifier = {
			add = 10
			scope:owner = {
				has_relation_soldier_friend = scope:target
			}
		}
		modifier = {
			add = 20
			scope:owner = {
				has_relation_best_friend = scope:target
			}
		}
		modifier = {
			add = 10
			scope:owner = {
				has_relation_lover = scope:target
			}
		}
		modifier = {
			add = 20
			scope:owner = {
				has_relation_soulmate = scope:target
			}
		}
		# TARGET #
		modifier = {
			add = -10
			scope:owner = {
				has_trait = calm
			}
		}
		modifier = {
			add = -20
			scope:owner = {
				has_trait = stubborn
			}
		}
		modifier = {
			add = 20
			scope:owner = {
				has_trait = fickle
			}
		}
		# DISCOVERY #
		modifier = {
			add = -30
			always = scope:exposed
			desc = "SCHEME_IS_EXPOSED"
		}
	}
	on_ready = {
		scheme_owner = {
			trigger_event = anb_dominate_outcome.1
		}
	}
	on_monthly = {
		hostile_scheme_discovery_chance_effect = yes
		scheme_owner = {
			trigger_event = {
				on_action = anb_dominate_ongoing
				days = {
					1
					7
				}
			}
		}
	}
	on_invalidated = {
		if = {
			limit = {
				NOT = {
					scope:target.liege_or_court_owner = scope:owner.liege_or_court_owner
				}
			}
			scope:owner = {
				trigger_event = anb_spell_interruption.1
			}
		}
		else_if = {
			limit = {
				scope:target = {
					NOR = {
						is_imprisoned = no
						is_imprisoned_by = scope:owner
					}
				}
			}
			scope:owner = {
				trigger_event = anb_spell_interruption.2
			}
		}
		else_if = {
			limit = {
				scope:target = {
					is_alive = no
				}
			}
			scope:owner = {
				trigger_event = anb_spell_interruption.3
			}
		}
		else_if = {
			limit = {
				scope:target = {
					has_trait = incapable
				}
			}
			scope:owner = {
				trigger_event = anb_spell_interruption.4
			}
		}
		else_if = {
			limit = {
				scope:owner = {
					has_strong_hook = scope:target
				}
			}
			scope:owner = {
				trigger_event = anb_spell_interruption.11
			}
		}
	}
	success_desc = "anb_dominate_success_desc"
	discovery_desc = "anb_dominate_discovery_desc"
}
