﻿name_list_dwarven = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {
		Adar Adranbur Adun Balgar Balgor Balin Bardin Bardur Beldun Belgar Bofur Bolin Bombur Borin Brann Brok Bruenor Calin Cun Dagran Dain Dalbaran Dalmund Dolin Dorin Dorri Durbur Durin Dwalin Dwarin Edun Falstad 
		Farun Fimbur Flondi Foldan Gorar Goren Gotrek Grim Grimmar Grimmun Gromar Gronmar Gruaim Gruar Grun Gundobad Gundobar Gundobin Gundomar Hardun Harlun Harruk Hjal Hjalmar Karrom Kazadur Khadan Kildan Kildar Kildum Kurgum Lodun Lorek Lorimbur Madarun Magar 
		Magni Magnus Magun Morzad Muradin Nali Nalin Norbun Norbur Nori Norimbur Norin Odar Odur Odun Okar Olin Orim Orin Ragun Ragund Runmar Runimur Sevrund Simbur Simur Simun Sindri Snorri Storin Storlin Storum Thargas Thargor Thinbur Thindobar Thindri 
		Thingrim Thinobad Thorar Thorgrim Thorin Thrain Thrair Thror Thrun Thrur Thudri Tungdil Ungrim Urist Urur
	}
	female_names = {
		Adala Aira Anvi Auci Balga Barila Barra Belga Belgia Bella Branma Brenla Brenleil Brenlel Brina Broga Bronis Cala Dalina 
		Dwalra Edeth Etta Grelba Grimma Groria Gruia Isondi Lahgi Lisban Magda Mistri Moira Mori Moria Naim Ovondi Ronla Sivondi Tharkuni Thohga Ungi
	}
	dynasty_of_location_prefix = "dynnp_az"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 40
	father_name_chance = 0
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 30
	mat_grm_name_chance = 30
	mother_name_chance = 0

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}
