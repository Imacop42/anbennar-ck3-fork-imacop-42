﻿language_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_common }
			multiply = 10
		}
	}
	
	color = { 222 222 222 }
}

language_bulwari = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_bulwari
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_bulwari }
			multiply = 10
		}
	}
	
	color = { 222 222 0 }
}

language_kheteratan = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_kheteratan
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_kheteratan }
			multiply = 10
		}
	}
	
	color = { 255 128 14 }
}

#a kheteratan dialect
language_ekhani = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_ekhani
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_ekhani }
			multiply = 10
		}
	}
	
	color = { 206 222 199 }
}

#a kheteratan dialect
language_deshaki = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_deshaki
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_deshaki }
			multiply = 10
		}
	}
	
	color = { 50 165 195 }
}

#a kheteratan dialect
language_khasani = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_khasani
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_khasani }
			multiply = 10
		}
	}
	
	color = { 90 135 65 }
}

language_gerudian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_gerudian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_gerudian }
			multiply = 10
		}
	}
	
	color = { 163 211 222 }
}

language_dwarven = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_dwarven
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_dwarven }
			multiply = 10
		}
	}
	
	color = { 128 128 128 }
}

#Should elven be divided? for now its not
language_elven = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_elven
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_elven }
			multiply = 10
		}
	}
	
	color = { 128 240 240 }
}

language_old_castanorian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_old_castanorian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_old_castanorian }
			multiply = 10
		}
	}
	
	color = { 255 255 255 }
}

language_gnomish = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_gnomish
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_gnomish }
			multiply = 10
		}
	}
	
	color = { 255 105 180 }
}