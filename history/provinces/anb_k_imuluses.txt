#k_imuluses
##d_imuluses
###c_eonkavan
551 = {		#Eonkavan

    # Misc
    culture = sun_elvish
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_tunkalem
553 = {		#Tunkalem

    # Misc
    culture = sun_elvish
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_kaboustraz
552 = {		#Kaboustraz

    # Misc
    culture = kuzarami
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_sad_kuz
###c_sad_kuz
556 = {		#Sad Kuz

    # Misc
    culture = kuzarami
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_kuztanuz
555 = {		#Kuztanuz

    # Misc
    culture = kuzarami
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_tuklum_elum
554 = {		#Tuklum Elum

    # Misc
    culture = kuzarami
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_barzilsad
557 = {		#Barzilsad

    # Misc
    culture = kuzarami
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}