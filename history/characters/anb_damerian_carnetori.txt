﻿7000 = { # Westyn I "the Elder" of Wesdam
	name = "Westyn"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = honest
	trait = impatient
	trait = diligent
	trait = education_stewardship_2
	
	father = 7003
	mother = 7004
	
	986.12.17 = {
		birth = yes
	}
	
	1005.2.5 = {
		add_spouse = 7001
	}
	
	1018.4.13 = {
		death = {
			death_reason = death_duel
			killer = 50018 # Trutgard of Marllin
		}
	}
}

7001 = { # Amelie, wife of Westyn "the Elder"
	name = "Amelie"
	dynasty_house = house_roilsard
	religion = court_of_adean
	culture = roilsardi
	female = yes
	
	trait = race_human
	trait = gregarious
	trait = patient
	trait = content
	trait = education_diplomacy_1
	
	father = 504
	
	988.4.11 = {
		birth = yes
	}
	
	1005.2.5 = {
		add_spouse = 7000
	}
}

7002 = { # Westyn II "the Younger" of Wesdam
	name = "Westyn"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = shy
	trait = humble
	trait = chaste
	trait = education_learning_2
	
	father = 7000
	mother = 7001
	
	1005.11.2 = {
		birth = yes
	}
}

7003 = { # Ricard I of Wesdam
	name = "Ricard"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = wrathful
	trait = cynical
	trait = stubborn
	trait = education_martial_3
	
	father = damerian7009
	
	964.6.13 = {
		birth = yes
	}
	
	990.11.26 = {
		add_spouse = 7004
	}
	
	1014.7.24 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

7004 = { # Cora, wife of Ricard
	name = "Cora"
	dynasty = dynasty_gabalaire
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = humble
	trait = shy
	trait = forgiving
	trait = depressed_1
	trait = education_learning_2

	968.4.30 = {
		birth = yes
	}
	
	990.11.26 = {
		add_spouse = 7004
	}
}

damerian7005 = { # William I "the Westward" of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = diligent
	trait = brave
	trait = calm
	trait = education_martial_4
	trait = unyielding_defender
	trait = strategist
	trait = overseer
	
	861.3.12 = {
		birth = yes
	}
	
	898.5.2 = {
		give_nickname = nick_the_westward
	}
	
	915.12.20 = {
		death = "915.12.20"
	}
}

damerian7006 = { # Caylen of Wesdam
	name = "Caylen"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = temperate
	trait = generous
	trait = humble
	trait = education_stewardship_4
	trait = administrator
	
	father = damerian7005
	
	878.7.18 = {
		birth = yes
	}
	
	934.2.23 = {
		death = {
			death_reason = "934.2.23"
		}
	}
}

damerian7007 = { # William II of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = fickle
	trait = gregarious
	trait = gluttonous
	trait = education_diplomacy_2
	
	father = damerian7006
	
	900.5.31 = {
		birth = yes
	}
	
	945.3.26 = {
		death = {
			death_reason = "945.3.26"
		}
	}
}

damerian7008 = { # Devan of Wesdam
	name = "Devan"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = cynical
	trait = callous
	trait = stubborn
	trait = education_learning_1
	
	father = damerian7007
	
	919.1.18 = {
		birth = yes
	}
	
	974.3.25 = {
		death = {
			death_reason = "974.3.25"
		}
	}
}

damerian7009 = { # William III of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = honest
	trait = diligent
	trait = forgiving
	trait = education_stewardship_2
	
	father = damerian7008
	
	943.10.6 = {
		birth = yes
	}
	
	990.11.25 = {
		death = {
			death_reason = death_consumption
		}
	}
}

damerian7010 = { # Petran, Trutgaud's father
	name = "Petran"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = arbitrary
	trait = cynical
	trait = ambitious
	
	father = damerian7008
	
	950.4.2 = {
		birth = yes
	}
	
	994.6.17 = { # Treachery at Tomansford
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

damerian7011 = { # Daran Sil Cestir
	name = "Daran"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_martial_3
	trait = education_martial_prowess_3
	trait = reckless
	trait = brave
	trait = impatient
	trait = arbitrary
	
	985.1.1 = {
		birth = yes
	}
}

damerian7012 = { # Lain Sil Cestir, first chiled of Daran
	name = "Lain"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_diplomacy_2
	trait = temperate
	trait = compassionate
	trait = calm
	trait = one_legged
	
	father = damerian7011
	mother = damerian7015
	
	1004.1.1 = {
		birth = yes
	}
}

damerian7013 = { # Toman Sil Cestir, second child of Daran
	name = "Toman"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_intrigue_2
	trait = fickle
	trait = arrogant
	trait = callous
	
	father = damerian7011
	mother = damerian7015
	
	1006.1.1 = {
		birth = yes
	}
}

damerian7014 = { # Auci Sil Cestir, third child of Daran
	name = "Auci"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = curious
	trait = trusting
	trait = patient
	
	father = damerian7011
	mother = damerian7015
	
	1010.1.1 = {
		birth = yes
	}
}

damerian7015 = { # Dalya Cannleis, Daran's wife
	name = "Dayla"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = generous
	trait = ambitious
	trait = cynical
	
	father = damerian7016
	
	987.1.1 = {
		birth = yes
	}
}

damerian7016 = { # Godric Cannleis. count of Cannleigh
	name = "Godric"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_learning_1
	trait = content
	trait = lazy
	trait = craven
	
	957.1.1 = {
		birth = yes
	}
}